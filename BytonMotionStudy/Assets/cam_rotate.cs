﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cam_rotate : MonoBehaviour {
	public Transform target;
	public float speed = 1;
	public float y,x = 1;

	public bool arrange;
	public float diameter = 100;
	public float angle_delta = 20;
	static public int id = 0;
	static public float focus_offset = 0;
	public int my_id = 0;
	public float offset = 1;
	public float oval_x, oval_y;
	public float my_focus_offset = 0;
	public static bool active = true;
	public static float z_limit = 100;
	// Use this for initialization
	void Start () {
		if (arrange) {
			//my_id = id; id++;
			transform.position = target.position + new Vector3 (0, diameter, 0);
			transform.RotateAround (target.position, Vector3.right, offset +  (my_id * angle_delta));
			transform.rotation = Quaternion.Euler( new Vector3(0,0,0));
		}
	}
	
	// Update is called once per frame
	Vector3 pos = new Vector3();
	void Update () {
		if (arrange) {
			if (Input.mousePosition.x > Screen.width * 0.25)
				return;
			float _speed =  speed * (Input.mousePosition.y - (Screen.height * 0.5f));
			transform.RotateAround (target.position, Vector3.right, _speed);
			pos = transform.position;


			if (oval_x != 0)
				pos.y += oval_x * pos.z;
			if (oval_y != 0)
				pos.z += oval_y * pos.y;


		
			transform.position = pos;
			if (oval_x != 0)
				pos.y -= oval_x * pos.y;
			if (oval_y != 0)
				pos.z -= oval_y * pos.y; 
		} else {
			Vector3 axis = new Vector3 (Input.mousePosition.y - (Screen.height * 0.5f), (Input.mousePosition.x - (Screen.width * 0.5f)), 0).normalized;
			axis = new Vector3 (axis.x * x, axis.y * y, 0);
			transform.RotateAround (target.position, axis, speed);
		}
			
		transform.rotation = Quaternion.Euler( new Vector3(0,0,0));
	}

	void OnMouseDown(){
		Debug.Log (my_id);
	}

	void OnMouseOver(){
		Debug.Log (my_id);
	}

	void OnMouseLeave(){
		Debug.Log (my_id);
	}

	public void SetFocusedOffset(float amount){
		transform.position = target.position + new Vector3 (0, diameter, 0);
		transform.RotateAround (target.position, Vector3.right, offset + amount +  (my_id * angle_delta));
		focus_offset = amount;
	}
}
